<?php

namespace JyDianping\Dianping;

use JyDianping\Kernel\Http;

trait Shop
{
  /**
   * 通过code换取店铺session换取接口
   * http://open.dianping.com/document/v2?docId=6000341&rootDocId=5000
   *
   * @param $code
   * @param $redirect_url
   */
  public function getShopSession($code, $redirect_url)
  {
    $data = [
      'app_key'      => $this->config['key'],
      'app_secret'   => $this->config['secret'],
      'auth_code'    => $code,
      'grant_type'   => 'authorization_code',
      'redirect_url' => $redirect_url,
    ];
    return Http::httpPost('https://openapi.dianping.com/router/oauth/token', $data);
    // array (
    //   'code' => 200,
    //   'msg' => 'success',
    //   'access_token' => 'f32993b0c7088eb316bf39ff5a00c2367451dacc',
    //   'expires_in' => 2591999,
    //   'remain_refresh_count' => 12,
    //   'tokenType' => 'bearer',
    //   'scope' => 'product,order',
    //   'bid' => '66fec22f3d1694f607aa5e361531c039',
    //   'refresh_token' => 'e760f509b6a937f0d63075d91c2d17b4dc417d15',
    // )
  }
  
  public function setShopSession($session)
  {
    $this->session = $session;
  }
  
  /**
   * session适用店铺查询接口
   * http://open.dianping.com/document/v2?docId=6000344&rootDocId=5000
   *
   * @param string $bid 客户的唯一标识，通过 getShopSession() 接口获得
   */
  public function getShopInfo($bid)
  {
    $data = [
      'bid'    => $bid,
      'offset' => 0,
      'limit'  => 100,
    ];
    $res  = $this->requestGet('/router/oauth/session/scope', $data);
    return $this->handleReturn($res);
  }
  
  /**
   * 获取门店优惠码
   *
   * @param $params
   * @return false|mixed
   */
  public function getShopCoupon($open_shop_uuid)
  {
    $params = ['open_shop_uuid' => $open_shop_uuid];
    return $this->requestGet('/router/poiqrcode/querydzcoupon', $params);
  }
}
