<?php

namespace JyDianping\Dianping;

trait Order
{
  /**
   * 取商品列表
   *
   * @param $params
   * @return mixed
   */
  public function getOrderInfo($params)
  {
    return $this->request('/router/order/queryorder', $params);
  }
  
  /**
   * 同步创建POS订单
   * http://open.dianping.com/document/v2?docId=6000429&rootDocId=5000
   *
   * @param $params
   * @return false|mixed
   */
  public function createPosOrder($params)
  {
    $error = [
      '1000416' => '店铺ID无权限',
      '1000417' => '处理中',
      '1000418' => '已经存在该订单',
    ];
    $res   = $this->request('/router/order/posordercreate', $params);
    if (isset($error[$res['code']])) {
      $res['msg'] .= '，' . $error[$res['code']];
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 平台请求第三方下单接口
   * http://open.dianping.com/document/v2?docId=6000433&rootDocId=4000
   *
   * @param $params
   * @return false|mixed
   */
  public function createThirdOrder($params)
  {
    return $this->request('/dzopen/ecommerce/order', $params);
  }
  
  /**
   * POS订单状态同步
   * http://open.dianping.com/document/v2?docId=6000430&rootDocId=5000
   */
  public function getPosOrderStatus()
  {
  
  }
}
