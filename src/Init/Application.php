<?php

namespace JyDianping\Init;

use JyDianping\Kernel\Response;
use JyDianping\Dianping\Dianping;
use JyDianping\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Dianping
{
  use Response;
  use BaseConfig;
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
